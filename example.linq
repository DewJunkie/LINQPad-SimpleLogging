<Query Kind="Statements">
  <NuGetReference>Linqpad.SimpleLogging</NuGetReference>
  <Namespace>LINQPad.SimpleLogging</Namespace>
  <Namespace>System.Reactive.Linq</Namespace>
  <Namespace>System.Threading.Tasks</Namespace>
</Query>

var keepRunning = Util.KeepRunning();
using (var l = new Logger(false))
using (new LINQPad.SimpleLogging.Timer("Let's time it"))
using(var test = new Tester(false))
{
    Observable.Timer(TimeSpan.FromSeconds(1), TimeSpan.FromMilliseconds(200))
        .Subscribe(tick =>
        {
            l.Log($"{tick} ticks, ah ah ah ah!!!");
            if (tick > 5 && keepRunning != null)
            {
                keepRunning.Dispose();
                keepRunning = null;
            }
        });
    "Doing some stuff".Dump();

    test.Test(1 > 1, "Should Fail");
    test.Test(() =>
    {
        throw new Exception("Failed");
    }, "Should Fail");

    while(keepRunning != null)
    {
     Thread.Sleep(250);
     "Still waiting".Dump();
    }

    test.Test(() =>
    {
        Debug.Assert(1 == 3, "assert message");
    }, "Should Pass");
    test.Test(1 == 1, "Should Pass");
}