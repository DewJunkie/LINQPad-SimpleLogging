﻿using System;
using System.Reactive;
using System.Threading.Tasks;

namespace LINQPad.SimpleLogging
{
    public class Tester:IDisposable
    {
        private int testCount=0;
        private int failedCount = 0;

        private readonly bool _stopOnFail;
        public Tester(bool stopOnFail = false)
        {
            this._stopOnFail = stopOnFail;
        }

        public async Task Test(Func<Task> test, string testDescription = default(string))
        {
            //$"Test Async called {testDescription}".Dump();
            testCount++;
            try
            {
                await test();
                Logger.Current.Log($"{testDescription} Passed");
            }
            catch (Exception e)
            {
                failedCount++;
                Logger.Current.Log($"{testDescription} Failed");
                if (_stopOnFail)
                    throw;

            }
            finally
            {
            }
        }

        public async Task Test(Action test, string testDescription = default(string))
        {
            //$"Test Normal Called {testDescription}".Dump();
            await Test(async () =>
            {
                test();
            }, testDescription);
        }

        public async Task Test(bool test, string testDescription = default(string))
        {
            await Test(async () =>
            {
                if (!test) throw new Exception($"{testDescription} Failed");
            }, testDescription);
        }

        public void Dispose()
        {
            Logger.Current.Log($"{testCount} Tests Ran {failedCount} Failing");
        }
    }
}
