﻿using System;
using System.Diagnostics;

namespace LINQPad.SimpleLogging
{
    public class Timer:IDisposable
    {
        public Timer(string message=default(string))
        {
            _message = message;
        }
            private readonly string    _message;
            private readonly Stopwatch sw = Stopwatch.StartNew();

            public void Dispose()
            {
                Logger.Current.Log($"{sw.Elapsed}:{_message} Finished.");
                sw.Stop();
            }
        }
}