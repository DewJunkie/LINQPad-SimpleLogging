﻿using System;
using System.Reactive.Subjects;

namespace LINQPad.SimpleLogging
{
    public class Logger : IDisposable
    {
        private static Logger _current;

        private readonly Subject<LogEvent> log = new Subject<LogEvent>();

        public Logger(bool newPane = false)
        {
            _current = this;
            log.Dump(null, null, newPane, null, false);
        }

        public static Logger Current => _current ?? new Logger();

        public void Log(object o)
        {
            log.OnNext(new LogEvent($"{o}"));
        }

        public class LogEvent
        {
            public LogEvent(string message = null)
            {
                Message = message;
            }
            public DateTime Time { get; set; } = DateTime.Now;
            public TimeSpan Elapsed { get; set; } = Util.ElapsedTime;
            public string   Message { get; set; }
        }

        #region IDisposable Support

        private bool disposedValue; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                    log.OnCompleted();
                    if (_current == this) _current = null;
                }
                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~Log() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

        #endregion
    }
}