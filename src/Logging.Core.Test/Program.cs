﻿using System;

namespace Logging.Core.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            using (new Logging.Logger())
                using(new Logging.Timing("It's alive!"))
            {
                Console.WriteLine("Hello World!");
            }
        }
    }
}
