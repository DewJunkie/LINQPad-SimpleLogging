```cs
var keepRunning = Util.KeepRunning();
using (var l = new Logger(false))
using (new LINQPad.SimpleLogging.Timer("Let's time it"))
using(var test = new Tester(false))
{
    Observable.Timer(TimeSpan.FromSeconds(1), TimeSpan.FromMilliseconds(200))
        .Subscribe(tick =>
        {
            l.Log($"{tick} ticks, ah ah ah ah!!!");
            if (tick > 5 && keepRunning != null)
            {
                keepRunning.Dispose();
                keepRunning = null;
            }
        });
    "Doing some stuff".Dump();

    test.Test(1 > 1, "Should Fail");
    test.Test(() =>
    {
        throw new Exception("Failed");
    }, "Should Fail");

    while(keepRunning != null)
    {
     Thread.Sleep(250);
     "Still waiting".Dump();
    }

    test.Test(() =>
    {
        Debug.Assert(1 == 3, "assert message");
    }, "Should Pass");
    test.Test(1 == 1, "Should Pass");
}
```
Separate logging messages from the rest of your query.  
# IObservable \<LogEvent\>

|Time                   |Elapsed            |Message                                    |
|-                      |-                  |-                                          |
|11/30/2017 11:17:44 AM | 00:00:00.0012208  | Should Fail Failed                        |
|11/30/2017 11:17:44 AM | 00:00:00.0015077  |Should Fail Failed                         |
|11/30/2017 11:17:45 AM | 00:00:01.0016938  |0 ticks, ah ah ah ah!!!                    | 
|11/30/2017 11:17:45 AM | 00:00:01.2133609  |1 ticks, ah ah ah ah!!!                    | 
|11/30/2017 11:17:45 AM | 00:00:01.4231317  |2 ticks, ah ah ah ah!!!                    | 
|11/30/2017 11:17:46 AM | 00:00:01.6365158  |3 ticks, ah ah ah ah!!!                    | 
|11/30/2017 11:17:46 AM | 00:00:01.8514935  |4 ticks, ah ah ah ah!!!                    | 
|11/30/2017 11:17:46 AM | 00:00:02.0515280  |5 ticks, ah ah ah ah!!!                    | 
|11/30/2017 11:17:46 AM | 00:00:02.2526261  |6 ticks, ah ah ah ah!!!                    | 
|11/30/2017 11:17:46 AM | 00:00:02.2572825  |Should Pass Passed                         |
|11/30/2017 11:17:46 AM | 00:00:02.2574166  |Should Pass Passed                         |
|11/30/2017 11:17:46 AM | 00:00:02.2574634  |4 Tests Ran 2 Failing                      |
|11/30/2017 11:17:46 AM | 00:00:02.2575174  |00:00:02.2563973:Let's time it Finished.   | 

Starting do so some stuff  
Doing some stuff  
Still waiting  
Still waiting  
Still waiting  
Still waiting  
Still waiting  
Still waiting  
Still waiting  
Still waiting  
Still waiting  
Fail: assert message  